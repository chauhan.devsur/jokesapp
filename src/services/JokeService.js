import axios from "axios";
var config = { headers: { Accept: "application/json" } };
export default class Jokeservice {
  getRandomJoke() {
    const apiURL = "https://icanhazdadjoke.com/";
    return axios.get(apiURL, config);
  }
}
