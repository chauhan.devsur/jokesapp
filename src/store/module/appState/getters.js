import { GET_JOKES, GET_SERVICE_RESPONSE } from "./getter-types";

export const getters = {
  [GET_JOKES]: (state) => state.jokes,
  [GET_SERVICE_RESPONSE]: (state) => state.serviceRespondedCounter,
};
