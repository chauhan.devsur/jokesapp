import { FETCH_JOKE } from "./action-types";
import JokeService from "../../../services/JokeService";
import {
  UPDATE_JOKE,
  INCREMENT_SERVICE_COUNTER,
  DECREMENT_SERVICE_COUNTER,
} from "./mutation-types";

const jokeService = new JokeService();

export const actions = {
  [FETCH_JOKE]({ commit }) {
    commit(INCREMENT_SERVICE_COUNTER);
    return jokeService
      .getRandomJoke()
      .then((res) => {
        if (res.status !== 200) {
          commit(DECREMENT_SERVICE_COUNTER);
          throw new Error(`${res.status} error when fetching token!`);
        } else {
          commit(UPDATE_JOKE, res);
          commit(DECREMENT_SERVICE_COUNTER);
          return res;
        }
      })
      .catch(() => {
        commit(UPDATE_JOKE, "error");
        commit(DECREMENT_SERVICE_COUNTER);
      });
  },
};
