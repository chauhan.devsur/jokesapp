import {
  UPDATE_JOKE,
  INCREMENT_SERVICE_COUNTER,
  DECREMENT_SERVICE_COUNTER,
  CLEAR_STATE,
} from "./mutation-types";

export const mutations = {
  [UPDATE_JOKE](state, payload) {
    if (localStorage.getItem("jokes")) {
      let existingJokes = JSON.parse(localStorage.getItem("jokes")) || [];
      existingJokes.push(payload.data.joke);
      localStorage.setItem("jokes", JSON.stringify(existingJokes));
      state.jokes = existingJokes;
    } else {
      const jokeObj = [];
      jokeObj.push(payload.data.joke);
      localStorage.setItem("jokes", JSON.stringify(jokeObj));
      state.jokes = jokeObj;
    }
  },
  [INCREMENT_SERVICE_COUNTER](state) {
    state.serviceRespondedCounter++;
  },
  [DECREMENT_SERVICE_COUNTER](state) {
    state.serviceRespondedCounter--;
  },
  [CLEAR_STATE](state) {
    state.jokes = "";
  },
};
