import { actions } from "./actions";
import { getters } from "./getters";
import { mutations } from "./mutation";

const appState = {
  state: {
    jokes: JSON.parse(window.localStorage.getItem("jokes")),
    serviceRespondedCounter: 0,
  },
  actions,
  getters,
  mutations,
};
export default appState;
