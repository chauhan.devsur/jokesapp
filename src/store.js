import Vue from "vue";
import Vuex from "vuex";
import appState from "./store/module/appState/index";
Vue.use(Vuex);
// import createpersistedstate from "vuex-persistedstate";

export default new Vuex.Store({
  modules: {
    appState,
    // plugins: [
    //   createpersistedstate({
    //     storage: window.sessionstorage,
    //     reducer(data) {
    //       return {
    //         //Set to store only mydata in state
    //         jokes: data.jokes,
    //       };
    //     },
    //   }),
    // ],
  },
});
