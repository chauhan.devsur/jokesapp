# jokes-app

Vue version--2.6.11

## Project setup

```
yarn install
```

### Dependencies for bootstrap vue

vue add Bootstrap-vue

### Compiles and hot-reloads for development

```
yarn serve
```

### Compiles and minifies for production

```
yarn run build
```

### Run your tests

```
yarn run test
```

### Lints and fixes files

```
yarn run lint
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).

## Application screenshots
